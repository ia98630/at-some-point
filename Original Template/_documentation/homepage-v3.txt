[wpjobus_recent_resumes]

[register_accounts employer_title="Employers" employer_desc="Register an employer account, create your company's profile and start publish awesome job offers." candidate_title="Candidates" candidate_desc="Register a candidate account, fill in your resume and increase your chances to get your dream job."]

[testimonials title="Testimonials" subtitle="Here’s what people are saying about our website." img_url=""]

[one_half_first]<h2>How it works</h2>
<h5>A job is a regular activity performed in exchange for payment. A person usually begins a job by becoming an employee, volunteering, or starting a business.</h5>
<p>The duration of a job may range from an hour (in the case of odd jobs) to a lifetime (in the case of some judges). The activity that requires a person's mental or physical effort is work (as in "a day's work"). If a person is trained for a certain type of job, they may have a profession. The series of jobs a person holds in their life is their career.</p>
[/one_half_first]

[one_half]<iframe width="480" height="360" src="//www.youtube.com/embed/-HROrImYIn8" frameborder="0" allowfullscreen></iframe>[/one_half]

[featured_companies title="Featured Companies" subtitle="Companies which are featured on our website. Some of them might recruit applicants!"]

[recent_news title="Recent News" subtitle="These are the recent news from our blog" button_text="Read older news" button_url="http://alexgurghis.com/themes/wpjobus/blog/"]